<?php
$coord = '10.481003, -66.850828';
?>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyC8mnUgDjVG46iS9GDh0x-vz7UWhuXEaA8&sensor=true"></script>
<script type="text/javascript">

    var map = null
    var marker = null

    jQuery(document).ready(function() {

        var locations = [
            ['Sede Principal', 10.4745955,-66.8597608, 2],
            ['Dirección Gráfica', 10.482198, -66.845470, 1],
        ];

        var mapOptions = {
            center: new google.maps.LatLng(<?php echo $coord ?>),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            navigationControl: false,
            scaleControl: false,
            optimized: false,
            styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}],
            zIndex:0
            /* HYBRID | ROADMAP | SATELLITE| TERRAIN */
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: "<?php echo get_template_directory_uri(); ?>/images/marker.png"
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }



    });
</script>
<div class="clearfix"></div>
<div class="map">
    <div id="map_canvas" class="map_gray" style="width:100%; height:270px; margin-bottom: 20px"></div>
</div>
<div class="clearfix"></div>
