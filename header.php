<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#b1b1b1" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#b1b1b1" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
       <meta name="author" content="Corporación D1" />
        <meta name="copyright" content="http://corporaciond1.com" />
        <meta name="geo.position" content="10.4756676,-66.8589413" />
        <meta name="ICBM" content="10.4756676,-66.8589413" />
        <meta name="geo.region" content="VE" />
        <meta name="geo.placename" content="Av. Paseo Eraso, Edif. Tamanaco, Piso 1- Oficina 1D, Las Mercedes, Caracas 1061" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <header class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div class="menu-btn-container">
                    <i id="menu-btn-open" class="fa fa-bars btn-show"></i>
                    <i id="menu-btn-close" class="fa fa-times btn-hide"></i>
                </div>
                <div class="menu-big-container menu-closed">
                    <div class="menu-big-social-container">
                        <a href="https://www.facebook.com/corporaciond1" target="_blank" title="Síguenos en Facebook"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.twitter.com/corporaciond1" target="_blank" title="Síguenos en Twitter"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.instagram.com/corporaciond1" target="_blank" title="Síguenos en Instagram"><i class="fa fa-instagram"></i></a>
                        <a href="https://www.behance.net/d1sign" target="_blank" title="Síguenos en Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
                    </div>
                    <ul>
                        <li><a id="menu-big-click-1" href="#inicio">Inicio</a></li>
                        <li><a id="menu-big-click-2" href="#booking">Booking</a></li>
                        <li><a id="menu-big-click-3" href="#branding">Branding</a></li>
                        <li><a id="menu-big-click-4" href="#diseno">Diseño</a></li>
                        <li><a id="menu-big-click-5" href="#grafica">Gráfica</a></li>
                        <li><a id="menu-big-click-6" href="#pr">PR</a></li>
                        <li><a id="menu-big-click-7" href="#procura">Procura</a></li>
                        <li><a id="menu-big-click-8" href="#produccion">Producción</a></li>
                        <li><a id="menu-big-click-9" href="#proyectos">Proyectos</a></li>
                        <li><a id="menu-big-click-10" href="#web">Web</a></li>
                        <li><a id="menu-big-click-11" href="#about">Somos</a></li>
                        <li><a id="menu-big-click-12" href="#behance">Behance</a></li>
                        <li><a id="menu-big-click-13" href="#cualquier">Cualquier cosa en Cualquier Momento</a></li>
                        <li><a id="menu-big-click-14" href="#clientes">Clientes</a></li>
                        <li><a id="menu-big-click-15" href="#footer">Ubicación</a></li>
                    </ul>
                </div>
                <div class="brochure-container">
                    <a href="http://corporaciond1.com/brochure_d1_af.pdf" title="Descarga nuestro brochure">
                        <button class="btn-md btn-custom-brochure">
                            Brochure <i class="fa fa-download"></i>
                        </button>
                    </a>
                </div>
            </div>
        </header>


