<?php get_header(); ?>
<?php the_post(); ?>

<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div id="fullpage">
            <section id="inicio" class="section the-hero the-services the-services-1 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="hero-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="logo-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-1 col-xs-12">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="Corporacion d1" class="img-responsive" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="booking" class="section the-services the-services-2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>BOOK<br />ING</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_09.png" alt="Booking" class="img-responsive" />
                            <p>Planificación y comercialización de presentaciones y shows. Booking de artistas nacionales e internacionales.</p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2Cbooking&text=BOOKING%20Planificación%20y%20comercializacion%20de%20presentaciones%20y%20shows&hashtags=d1%2Cbooking"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>
            <section id="branding" class="section the-services the-services-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>BRANDING</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_03.png" alt="Branding" class="img-responsive" />
                            <p>Concebir marcas con carácter y creatividad. Se trata de generar repercusión gráfica, captación y fidelización a través de la identidad de nuestros clientes.</p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2Cbranding&text=BRANDING%20concebir%20marcas%20con%20caracter%20y%20creatividad&hashtags=d1%2Cbranding"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>
            <section id="diseno" class="section the-services the-services-4 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>DISE<br />ÑO</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_01.png" alt="Diseño" class="img-responsive" />
                            <p>Equilibrio entre función y forma. Todo, en el mundo, está diseñado. Conocimiento, inspiración, funcionabilidad e innovación se conjugan en favor de los resultados.</p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2Cdiseño&text=DISEÑO%20Equilibrio%20entre%20función%20y%20forma&hashtags=d1%2Cdiseño"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>

            <section id="grafica" class="section the-services the-services-5 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>GRÁ<br />FICA</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_08.png" alt="Gráfica" class="img-responsive" />
                            <p>Brinda las mejores tecnologías para llevar un archivo digital a impresiones en diferentes tipos de formatos y material, todo esto en un mínimo tiempo de ejecución. Con equipos de primera línea y en constante mantenimiento y actualización para la impresión.</p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2Cgráfica&text=GRÁFICA%20Brinda%20tecnologías%20para%20llevar%20un%20archivo%20digital%20a%20impresiones%20en%20diferentes%20formatos&hashtags=d1%2Cgráfica"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>
            <section id="pr" class="section the-services the-services-6 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>PR</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_05.png" alt="PR" class="img-responsive" />
                            <p>Asesoría estratégica dentro de cualquier entorno corporativo, público y privado, para brindar siempre soluciones acertadas. </p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2CPR&text=PR%20Asesoría%20estratégica%20dentro%20de%20cualquier%20entorno%20corporativo&hashtags=d1%2CPR"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>
            <section id="procura" class="section the-services the-services-7 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>PRO<br />CURA</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_07.png" alt="Branding" class="img-responsive" />
                            <p>Solucionamos cualquier requerimiento circunstancial que requieran nuestros clientes gracias a la planificación y logística de nuestro equipo humano.</p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2CPROCURA&text=PROCURA%20Solucionamos%20cualquier%20requerimiento%20circunstancial%20que%20requieran%20nuestros%20clientes&hashtags=d1%2CPROCURA"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>
            <section id="produccion" class="section the-services the-services-8 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>PRO<br />DUC<br />CIÓN</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_06.png" alt="Branding" class="img-responsive" />
                            <p>LED, backing, logística, protocolo; así como renders para que disfrutes de una perspectiva del modelo o escenario de tu evento en 3D. Todo en sonido e Iluminación profesional, diseño de tarimas, montajes y ejecución de proyectos. Sistemas de Video Mapping y pisos interactivos.</p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2CPROCURA&text=PROCURA%20Solucionamos%20cualquier%20requerimiento%20circunstancial%20que%20requieran%20nuestros%20clientes&hashtags=d1%2CPROCURA"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>
            <section id="proyectos" class="section the-services the-services-9 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>PRO<br />YEC<br />TOS</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_04.png" alt="Proyectos" class="img-responsive" />
                            <p>Gracias a especialistas en el área de la arquitectura y la ingeniería civil en d1 emprendemos proyectos, desde su diseño hasta su ejecución, en materia de obra civil.</p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2CPROYECTOS&text=PROYECTOS%20emprendemos%20proyectos%20desde%20su%20diseño%20hasta%20su%20ejecución&hashtags=d1%2CPROYECTOS"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>
            <section id="web" class="section the-services the-services-10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <h2>WEB</h2>
                        </div>
                        <div class="info-container col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/service_02.png" alt="Web" class="img-responsive" />
                            <p>Especialistas en desarrollo de tema en WordPress. Desarrollo de Plugins en WordPress: un área personalizada donde puedas llevar la gestión de tu empresa en tu página web y además ofrecemos asesoría e implementación de SEO / SEM para que tu marca aparezca en los principales navegadores.</p>
                            <iframe
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=https%3A%2F%2Fcorporaciond1.com&via=corporaciond1&related=d1%2CWEB&text=WEB%20Especialistas%20en%20desarrollo%20de%20tema%20en%20WordPress&hashtags=d1%2CWEB"
                                    width="160"
                                    height="30"
                                    title="Twitter Tweet Button"
                                    style="border: 0; overflow: hidden;">
                            </iframe>
                        </div>
                    </div>
                </div>
            </section>

            <section id="about" class="section the-services the-services-11 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container letter-360 col-lg-8 col-md-8 col-sm-8 col-xs-6 paddingr40">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo360.png" alt="Behance d1" class="img-responsive">
                        </div>
                        <div class="info-container info-about col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <p>d1 apunta a la eficacia de la comunicación y al cumplimiento del objetivo de nuestros clientes, logrando aumentar su posicionamiento y la ejecución de todos (sí, todos) sus requerimientos.</p>

                        </div>
                    </div>
                </div>
            </section>
            <section id="behance" class="section the-services the-services-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="letter-container  col-lg-8 col-md-8 col-sm-8 col-xs-6">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/behance-d1.png" alt="Behance d1" class="img-responsive">
                        </div>
                        <div class="info-container info-behance col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <a href="https://www.behance.net/d1sign" target="_blank" title="Síguenos en Behance">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/be.png" alt="Powered by behance" class="img-responsive img-behance" />
                            </a>
                            <p>Ingresa y descubre en nuestro portafolio Behance los proyectos más resaltantes de nuestros principales clientes. </p>
                        </div>
                    </div>
                </div>
            </section>


            <section id="cualquier" class="section the-services the-services-13 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="slogan-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/slogan-spa.png" alt="cualquier cosa en cualquier momento" class="img-responsive" />
                        </div>
                        <div class="info-container info-container2 col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6 col-xs-12">
                            <p>para nosotros la inmediatez es nuestro valor fundamental: comunicar y dar respuesta con eficiencia a cualquier cosa en cualquier momento</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>
            <section id="clientes" class="section the-services the-services-14  the-clients col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="row">
                        <section class="home-clients-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="home-clients-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="letter-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h2>CLIENTES</h2>
                                </div>
                                <div class="home-client-slider-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-01.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-02.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-03.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-04.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-05.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-06.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-07.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-08.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-09.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-10.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-11.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-12.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-13.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-14.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-15.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-16.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-17.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-18.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-19.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-20.png" alt="Clientes" class="img-responsive" /></div>
                                    <div class="home-client-slider-item"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clients/client-21.png" alt="Clientes" class="img-responsive" /></div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <div id="footer" class="section the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <?php get_template_part('templates/map');?>
                <div class="container">
                    <div class="row">

                        <div class="footer-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <p>Av. Paseo Eraso, Edificio Tamanaco, Piso 1, Oificina 1D. Las Mercedes Caracas 1061</p>
                            <p>+58 212 993-5903 / +58 212 991-4989</p>
                            <p>Chuao. Avenida principal del Cafetal. Edificio Clínica. Parcela N.147</p>
                            <p>+58 212 993-5903</p>
                            <p>Ver Dirección en <a href="                       https://www.google.co.ve/maps/place/deunomedia/@10.4757402,-66.8586016,17z/data=!4m13!1m7!3m6!1s0x8c2a58f5d8d954ff:0x839fa99ee6b41bb9!2sPaseo+Enrique+Eraso,+Caracas,+Distrito+Capital!3b1!8m2!3d10.4774543!4d-66.8579065!3m4!1s0x0:0x73a544d64cbac934!8m2!3d10.4758207!4d-66.8583921!6m1!1e1?hl=en">Google Maps</a></p>
                            <div class="footer-social col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl">
                                <a href="https://www.facebook.com/corporaciond1" target="_blank" title="Síguenos en Facebook"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.twitter.com/corporaciond1" target="_blank" title="Síguenos en Twitter"><i class="fa fa-twitter"></i></a>
                                <a href="https://www.instagram.com/corporaciond1" target="_blank" title="Síguenos en Instagram"><i class="fa fa-instagram"></i></a>
                                <a href="https://www.behance.net/d1sign" target="_blank" title="Síguenos en Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
                                <h5>Corporación D1 C.A. J-40346898-2</h5>
                            </div>
                        </div>
                        <div class="footer-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?php echo do_shortcode('[contact-form-7 id="226" title="Contacto"]'); ?>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <a href="http://www.publifestival.com/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/festival_d1.png" alt="" class="img-responsive" /></a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                <a href="http://www.elojodeiberoamerica.com/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/publi_d1.png" alt="" class="img-responsive" /></a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <a href="http://www.salonemilano.it/manifestazioni/salone-satellite.html" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/mobile-d1.png" alt="" class="img-responsive" /></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
<?php get_footer(); ?>
