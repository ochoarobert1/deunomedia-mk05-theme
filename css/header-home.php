<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//www.google-analytics.com"  />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#00ADE2" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#b1b1b1" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="Corporación D1" />
        <meta name="copyright" content="http://corporaciond1.com" />
        <meta name="geo.position" content="10.4756676,-66.8589413" />
        <meta name="ICBM" content="10.4756676,-66.8589413" />
        <meta name="geo.region" content="VE" />
        <meta name="geo.placename" content="Av. Paseo Eraso, Edif. Tamanaco, Piso 1- Oficina 1D, Las Mercedes, Caracas 1061" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <div class="the-menu-home-container">
            <i onclick="openmenu()" class="fa fa-bars"></i>
        </div>
        <header class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div class="the-header the-header-home header-hide col-md-12 no-paddingl no-paddingr">
                    <a onclick="closemenu()" class="close-menu">x</a>
                    <nav class=" " role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php _e('deunomedia - cualquier cosa en cualquier momento', 'deunomedia' ); ?>" class="img-responsive" />
                        <?php wp_nav_menu(array( 'theme_location' => 'header_menu')); ?>
                    </nav>
                    <div class="the-menu-social col-md-12">
                        <a href="http://deunomedia.com">ES</a> / <a href="http://en.deunomedia.com">EN</a>
                        <div class="clearfix"></div>
                        <a href="https://www.facebook.com/corporaciond1" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.twitter.com/corporaciond1" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.instagram.com/corporaciond1" target="_blank"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </header>
